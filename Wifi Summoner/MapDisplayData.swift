//
//  MapDisplayData.swift
//  Wifi Summoner
//
//  Created by Andrew Scott on 8/1/15.
//  Copyright (c) 2015 Drowned Coast. All rights reserved.
//

import MapKit
import Cocoa

class MapDisplayData: NSObject, MKAnnotation {
    let title: String
    let subtitle: String
    let coordinate: CLLocationCoordinate2D
    var color: MKPinAnnotationColor
    
    init(ssid: String, IP: String, coordinate: CLLocationCoordinate2D) {
        self.title = "SSID: " + ssid
        self.subtitle = "Public IP: " + IP
        self.coordinate = coordinate
        self.color = MKPinAnnotationColor.Purple
        super.init()
    }
    
//    var subtitle: String {
//        return locationName
//    }
}
