//
//  SwiftBasher.swift
//  Wifi Summoner
//
//  Created by Andrew Scott on 7/16/15.
//  Copyright (c) 2015 Drowned Coast. All rights reserved.
//

import Foundation

class SwiftBasher {
    
    var currentPublicIP: String = ""
    var IPRegion: String = ""
    init() {}
    
    func runCommand(cmd : String, args : String...) -> (output: [String], error: [String], exitCode: Int32) {
        
        var output : [String] = []
        var error : [String] = []
        
        let task = NSTask()
        task.launchPath = cmd
        task.arguments = args
        
        let outpipe = NSPipe()
        task.standardOutput = outpipe
        let errpipe = NSPipe()
        task.standardError = errpipe
        
        task.launch()
        
        let outdata = outpipe.fileHandleForReading.readDataToEndOfFile()
        if var string = String.fromCString(UnsafePointer(outdata.bytes)) {
            string = string.stringByTrimmingCharactersInSet(NSCharacterSet.newlineCharacterSet())
            output = string.componentsSeparatedByString("\n")
        }
        
        let errdata = errpipe.fileHandleForReading.readDataToEndOfFile()
        if var string = String.fromCString(UnsafePointer(errdata.bytes)) {
            string = string.stringByTrimmingCharactersInSet(NSCharacterSet.newlineCharacterSet())
            error = string.componentsSeparatedByString("\n")
        }
        
       task.waitUntilExit()
        let status = task.terminationStatus
          
        return (output, error, status)
    }
    
    
    
    func getIPData(urlString: String) {
        let url = NSURL(string: urlString)
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {
            (data, response, error) in dispatch_async(dispatch_get_main_queue(),  {
                self.getIPAddress(data)
            })
        }
        task.resume()
        
        }
 
    func getIPAddress(IPData: NSData) {
        var result = NSString(data: IPData, encoding:
            NSASCIIStringEncoding)!
        let trimmedResult = result.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())

        self.currentPublicIP = trimmedResult as String
    }
    
    func getIPLocation(urlbase: String, newIP: String) {
        var fullURL = urlbase + newIP
        //println(fullURL)
        let url = NSURL(string: fullURL)
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {
            (data, response, error) in dispatch_async(dispatch_get_main_queue(),  {
                self.parseIPLocation(data)
            })
        }
        task.resume()
    }
    
    func parseIPLocation(IPData: NSData) {
        var jsonError = NSError?()
        
        var json = NSJSONSerialization.JSONObjectWithData(IPData, options: nil, error: &jsonError) as! NSDictionary
        
        if let region = json["region"] as? String {
            self.IPRegion = region
        }
    }
}
    
