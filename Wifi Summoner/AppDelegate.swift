//
//  AppDelegate.swift
//  Wifi Summoner
//
//  Created by Andrew Scott on 7/16/15.
//  Copyright (c) 2015 Drowned Coast. All rights reserved.
//

import Cocoa
import RealmSwift
import Foundation

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {


    @IBOutlet weak var window: NSWindow!

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }
    
    var masterViewController: MasterViewController!
    
    let statusItem = NSStatusBar.systemStatusBar().statusItemWithLength(-2)
    var sb = SwiftBasher()
    var locationFinder = LocationService()
    let realm = Realm() // Create realm pointing to default file

    var ssid = "Unknown"
    var psswd = "Unknown"
    var ssidLocation = "Unknown"
    var ssidIP = ""
    var ssidSecurity = ""
    var ssidMAC = ""
    let locatelatlon: NSMenuItem = NSMenuItem()
    var timeToWrite: Bool = false
    var readyToWrite: Bool = false
    var writeSuccess: Bool = false
    var currentlyConnected: Bool = false
    var currentNetwork:NSMenuItem = NSMenuItem()

    
    func applicationDidFinishLaunching(notification: NSNotification) {
        println(NSDate().descriptionWithLocale(nil))
        println(self.realm.path)
        // check to see if wifi is available
        if testWifiAtLaunch() {
            // if wifi is already connected we can fill in some things already
            if self.currentlyConnected && (checkIfRecordExists(self.ssid) == 2) {
                print("record exists, updating...\n")
                println(self.realm.path)
                self.psswd = self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",self.ssid))[0].password
                self.ssidLocation = self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",self.ssid))[0].location
            }
            else if self.currentlyConnected && (checkIfRecordExists(self.ssid) == 1){
                print("multiple records exist for ssid...\n")
            }
            //check if location is present, if not we'll ask for permission to find it
            if self.ssidLocation == "Unknown" {
                UpdateLocation()
            }
        }
        let menu = NSMenu()
        let titleItem:NSMenuItem = NSMenuItem()
        
        updateSSID()
        titleItem.title = "WIFI Summoner"
        menu.addItem(titleItem)
        menu.addItem(self.currentNetwork)
        menu.addItem(self.locatelatlon)
        
        
        menu.addItem(NSMenuItem.separatorItem())
        
        
        menu.addItem(NSMenuItem(title: "Save Current Network", action: Selector("saveCurrentSSID"), keyEquivalent: "s"))
        menu.addItem(NSMenuItem(title: "Stored Wifi Info", action: Selector("setWindowVisible:"), keyEquivalent: "d"))
        menu.addItem(NSMenuItem(title: "Quit", action: Selector("terminate:"), keyEquivalent: "q"))
        
        statusItem.menu = menu
        
        if let button = statusItem.button {
            button.image = NSImage(named: "StatusBarButtonImage")
        }
    }
    
    
    
    
    func expandWindow() {
        var oldWindowFrame: NSRect = self.window.frame
        var windowWidth = oldWindowFrame.width
        var oldWindowHeight = oldWindowFrame.height
        println(oldWindowHeight)
        if oldWindowHeight <= 323.0 {
        var newFrame = CGRectMake(oldWindowFrame.origin.x, oldWindowFrame.origin.y, windowWidth, oldWindowHeight+300)
        self.window.setFrame(newFrame, display: true)
        }
        else {
            var newFrame = CGRectMake(oldWindowFrame.origin.x, oldWindowFrame.origin.y, windowWidth, 322.0)
            self.window.setFrame(newFrame, display: true)
        }
    }
    
    
    
    
    func testWifiAtLaunch() -> Bool {
        var checkwifi = sb.runCommand("/bin/sh", args: "-c", "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk -F': ' '/ SSID/ {print $2}'")
        if checkwifi.output != [] {
            var launchSSID = checkwifi.output[0]
            self.currentlyConnected = true
            self.ssid = launchSSID
            return true
        }
        else {
            println("not connected to wifi network")
            return false
        }
        // figure out if SSID is already in our db
    }
    
    func findDetailed() -> [String] {
        
        var getDetails = sb.runCommand("/bin/sh", args: "-c", "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I")
        var detailsDict = [String: String]()
        var tempArray = [String]()
        if getDetails.output != [] {
           tempArray = (getDetails.output)
            for i in tempArray {
                var veryTempArray = [String]()
                veryTempArray = i.componentsSeparatedByString(": ")
                if veryTempArray.count == 2 {
                    detailsDict[veryTempArray[0]] = veryTempArray[1]
                }
            }
            println(detailsDict)
            return tempArray
        }
        return ["could not access airport"]
    }
    
    
    
    
    func UpdateLocation() {
        for i in 0...50 {
            if self.locationFinder.locationString == "0.0,0.0" || self.locationFinder.locationString == "" {
            self.locationFinder.execute()
            self.locatelatlon.title = self.locationFinder.displayLatLon(self.locationFinder.locationCurrent)
            }
        }
        if self.locationFinder.locationString == "0.0,0.0" || self.locationFinder.locationString == "" {
            self.locatelatlon.title = "Location Unavailable"
            self.ssidLocation = "Unknown"
        }
        else {
            self.ssidLocation = self.locationFinder.locationString
        }
    }
    
    
    
    
    
    func getWifiInfo() {
        if self.ssidLocation == "Unknown" {
            UpdateLocation()
        }
        // go get the ssid if connected
        sb.getIPData("http://www.telize.com/ip")
        
        var checkwifi = sb.runCommand("/bin/sh", args: "-c", "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk -F': ' '/ SSID/ {print $2}'")
        if checkwifi.output != [] {
            println(checkwifi.output)
            self.ssid = checkwifi.output[0]
            self.currentlyConnected = true
        }
        else if checkwifi.output == [] && self.currentlyConnected == true {
            println("this path now")
        }
        else {
            println("not connected to wifi network")
        }
        
        // if we're connected, go ahead and try to get the password
        if self.currentlyConnected {
            if self.psswd == "Unknown" {
                var checkpsswd = sb.runCommand("/bin/sh", args: "-c","security find-generic-password -ga \"\(self.ssid)\" | grep password:")
                if checkpsswd.error != [] {
                    self.psswd = checkpsswd.error[0].componentsSeparatedByString(" ")[1] //get the first elements back 
                    if self.psswd == "SecKeychainSearchCopyNext:" {
                        // there's no security, set this to "none"
                        self.psswd = "none"
                    }
                }
                else {
                    println("Password was not accessable")
                }
            }
        }
        
        
        //once we have these we can update the menu item
        updateSSID()
        
        // lets get some more shit if we're connected
        if self.currentlyConnected {
            // check to see if we already have the MAC
            if self.ssidMAC == "" {
                var checkbssid = sb.runCommand("/bin/sh", args: "-c", "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk -F': ' '/ BSSID/ {print $2}'")
                if checkbssid.output != [] {
                    self.ssidMAC = checkbssid.output[0]
                    //println(self.ssidMAC)
                }
                else {
                    println("could not access airport for bssid")
                }
            }
            
            // check to see if we already have security
            if self.ssidSecurity == "" {
                var checkbsec = sb.runCommand("/bin/sh", args: "-c", "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk -F': ' '/ link auth/ {print $2}'")
                if checkbsec.output != [] {
                    self.ssidSecurity = checkbsec.output[0]
                    println(checkbsec)
                    //println(self.ssidSecurity)
                }
                else {
                    println("could not access airport for security")
                }
            }
            
            
            // check to see if we have the IP yet
            if sb.currentPublicIP != "" {
                if (sb.currentPublicIP.lowercaseString.rangeOfString("<D") != nil) {
                    println("Could not resolve IP")
                    self.ssidIP = "Could not resolve IP"
                }
                else {
                    self.ssidIP = sb.currentPublicIP
                }
                
                
                // I could use this to get a very general location if location services fail
//                if self.ssidLocation == "" {
//                    sb.getIPLocation("http://www.telize.com/geoip/", newIP: sb.currentPublicIP)
//                    self.ssidLocation = sb.IPRegion
//                }
            }
            else {
                println("could not find public IP")
                // handle popup error
//                let alert = NSAlert()
//                alert.messageText = "Warning"
//                alert.addButtonWithTitle("OK")
//                alert.informativeText = "Could not find Public IP.  No internet connection."
//                
//                alert.beginSheetModalForWindow(self.window, completionHandler: nil)
            }
        }

        self.readyToWrite = self.currentlyConnected && (self.psswd != "Unknown" && self.ssidMAC != "" && self.ssidSecurity != "" && self.ssidIP != "" && self.ssidLocation != "") && !self.writeSuccess
        
        
        if self.timeToWrite && self.readyToWrite {
            writeToRealm()
            self.timeToWrite = false
            self.writeSuccess = true
        }
        
        
    }
    
    
    
    
    
    func updateSSID() {
        // set the details in the dropdown
        if self.currentlyConnected {
            self.currentNetwork.title = "SSID: " + self.ssid + " : " + self.psswd
        }
        else {
            self.currentNetwork.title = "Not currently connected"
        }
        self.locatelatlon.title = "Location: " + self.ssidLocation
    }
    
    
    
    
    func setWindowVisible(sender: AnyObject){
        // open the app window
        masterViewController = MasterViewController(nibName: "MasterViewController", bundle: nil)
        //self.window.makeKeyAndOrderFront(self)
        //self.window.makeKeyWindow()
        self.window.contentView.addSubview(masterViewController.view)
        masterViewController.view.frame = (self.window.contentView as! NSView).bounds
        self.window.orderFrontRegardless()
        self.window.makeKeyWindow()
        let predicate = NSPredicate(format: "ssid = %@",self.ssid)
        var resultsCurrent = self.realm.objects(SSIDObj).filter(predicate)
        var resultsAll = self.realm.objects(SSIDObj)
    }
    
    
    
    
    func saveCurrentSSID() {
        // set the flag to write, and repeat the lookup until we get what we need
        var timer = NSTimer()
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("getWifiInfo"), userInfo: nil, repeats: !self.readyToWrite)
        self.timeToWrite = true
        
        
    }
    
    func writeToRealm() {

        
        var toSave = SSIDObj() // create local model object
        toSave.ssid = self.ssid
        toSave.password = self.psswd
        toSave.security = self.ssidSecurity
        toSave.location = self.ssidLocation
        toSave.bssid = self.ssidMAC
        toSave.publicIP = self.ssidIP
        println(toSave)
        
        // Save your object
        self.realm.write {
            self.realm.add(toSave, update: true)
        }
        println(self.realm.path)
    }
    
    func checkIfRecordExists(ssid: String) -> Int {
        // check in realm to see if we already have an SSID
        if self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",self.ssid)).count > 1 {
            return 1
        }
        else if self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",self.ssid)).count > 0 {
            return 2
        }
        else {
            return 3
        }
        
    }
    
    
    
    
}

