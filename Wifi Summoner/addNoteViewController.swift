//
//  addNoteViewController.swift
//  Wifi Summoner
//
//  Created by Andrew Scott on 8/6/15.
//  Copyright (c) 2015 Drowned Coast. All rights reserved.
//

import Cocoa
import RealmSwift

class addNoteViewController: NSViewController, NSTextFieldDelegate {

    let appDelegate = NSApplication.sharedApplication().delegate as! AppDelegate
    let defaults = NSUserDefaults.standardUserDefaults()
    var realm = Realm()
    
    @IBOutlet weak var addNoteTextField: NSTextField!
    
    var noteString: String = ""
    var ssidIdentifier: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addNoteTextField.delegate = self
        if self.defaults.valueForKey("CurrentSelectedNetwordSSID") != nil {
            self.ssidIdentifier = self.defaults.valueForKey("CurrentSelectedNetwordSSID") as! String
        }
        var existingNote = self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",self.ssidIdentifier))[0].Notes
        if existingNote != "" {
            self.addNoteTextField.stringValue = existingNote
        }
        // Do view setup here.
    }
    
    override func controlTextDidEndEditing(obj: NSNotification) {
        println("done")
        self.noteString = addNoteTextField.stringValue
        println(self.appDelegate.checkIfRecordExists(self.ssidIdentifier))
        //self.realm.beginWrite()
        writeToRealm()
    }
    
    func writeToRealm() {
        var realmObj = self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",self.ssidIdentifier))[0]
        var toSave = SSIDObj()
        toSave.ssid = self.ssidIdentifier
        toSave.password = realmObj.password
        toSave.security = realmObj.security
        toSave.location = realmObj.location
        toSave.bssid = realmObj.bssid
        toSave.publicIP = realmObj.publicIP
        toSave.Notes = self.noteString
        self.realm.write {
            self.realm.add(toSave, update: true)
        }

    }
    
    override func viewDidAppear() {
        var existingNote = self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",self.ssidIdentifier))[0].Notes
        if existingNote != "" {
            self.addNoteTextField.stringValue = existingNote
        }
    }
    
    override func viewDidDisappear() {
        println("close popover")
    }
    
}
