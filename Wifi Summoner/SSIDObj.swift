//
//  SSIDDetails.swift
//  Wifi Summoner
//
//  Created by Andrew Scott on 7/17/15.
//  Copyright (c) 2015 Drowned Coast. All rights reserved.
//

import Foundation
import RealmSwift

class SSIDObj: Object {
    dynamic var ssid = ""
    dynamic var password = ""
    dynamic var security = ""
    dynamic var location = ""
    dynamic var bssid = ""
    dynamic var publicIP = ""
    dynamic var Export: Bool = true
    dynamic var Notes = ""
    dynamic var DateAdded: String = NSDate().descriptionWithLocale(nil)!
    dynamic var DateExported: String = NSDate().descriptionWithLocale(nil)!
    
    
    override class func primaryKey() -> String {
        return "bssid"}
}