//
//  MasterViewController.swift
//  Wifi Summoner
//
//  Created by Andrew Scott on 7/19/15.
//  Copyright (c) 2015 Drowned Coast. All rights reserved.
//

import Cocoa
import RealmSwift
import MapKit

class MasterViewController: NSViewController, MKMapViewDelegate {

    let realm = Realm() // Create realm pointing to default file
    var notificationToken: NotificationToken?
    let csv = CSVHandler()
    let addNotePopover = NSPopover()
    let appDelegate = NSApplication.sharedApplication().delegate as! AppDelegate
    let defaults = NSUserDefaults.standardUserDefaults()
    let sb = SwiftBasher()
    
    @IBOutlet weak var tableViewOutlet: NSTableView!
    @IBOutlet weak var tableViewCell: NSTextFieldCell!
    @IBOutlet weak var tableViewCheck: NSButtonCell!
    @IBOutlet var viewOutlet: NSView!
    @IBOutlet weak var deleteButton: NSButton!
    @IBOutlet weak var mapButton: NSButton!
    @IBOutlet weak var exportButton: NSButton!
    @IBOutlet weak var importButton: NSButton!
    @IBOutlet var mapview: MKMapView!
    @IBOutlet weak var detailsLabel: NSTextField!
    @IBOutlet weak var getDetailsButton: NSButton!
   
    
    
    
    var knownWifiLocations: [MKAnnotation] = []
    var isMapOpen: Bool = false
    var isNoteOpen: Bool = false
    var hasTableLoaded: Bool = false
    
    override func viewDidLoad() {
        println("view loaded")
        super.viewDidLoad()
        self.mapview.delegate = self
        
        
        // Do view setup here.
        self.mapview.hidden = true
        if self.realm.objects(SSIDObj).count != 0 {
        tableView(self.tableViewOutlet, viewForTableColumn: NSTableColumn?(),row: self.realm.objects(SSIDObj).count)
        }
        self.notificationToken = Realm().addNotificationBlock { [unowned self] note, realm in
            self.tableViewOutlet.reloadData() }
        
        if isNoteOpen {
            self.addNotePopover.performClose(self)
            self.isNoteOpen = false
        }
        
        var timer = NSTimer()
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "tableDidLoad", userInfo: nil, repeats: false)
    }
    
    func tableDidLoad() {
        self.hasTableLoaded = true
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 200
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        self.mapview.setRegion(coordinateRegion, animated: true)
    }
    
    
    override func mouseDown(theEvent: NSEvent) {
        if isNoteOpen {
            self.addNotePopover.performClose(self)
            self.isNoteOpen = false
        }
    }
    
    
    override func rightMouseDown(theEvent : NSEvent) {
        println("right mouse")
        println(theEvent.locationInWindow)
        // decide if we want to do right click stuff
        super.rightMouseDown(theEvent)
        var point: NSPoint = tableViewOutlet.convertPoint(theEvent.locationInWindow, fromView: nil)
        var row  = tableViewOutlet.rowAtPoint(point)
        println(row)
        var col = tableViewOutlet.columnAtPoint(point)
        var rec = tableViewOutlet.frameOfCellAtColumn(col, row: row)
 
        //figure out if there's data where the user clicked
        if row < numberOfRowsInTableView(tableViewOutlet) && row >= 0 {
            self.defaults.setValue(SelectedRow(row)?.ssid, forKey: "CurrentSelectedNetwordSSID")
            self.addNotePopover.contentViewController = addNoteViewController(nibName: "addNoteViewController", bundle: nil)
            self.addNotePopover.showRelativeToRect(rec, ofView: tableViewOutlet, preferredEdge: NSMinYEdge)
            self.isNoteOpen = true
            println("new popover")
        }
        else {
            if self.isNoteOpen{
                self.addNotePopover.performClose(self)
                self.isNoteOpen = false
            }
        }
        
    }
    

    
    @IBAction func deleteButton(sender: NSButton) {
        if SelectedRow() != nil {
            var ssidToDelete = SelectedRow()!.ssid as String
            var realmObjectToDelete = self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",ssidToDelete))
            println(realmObjectToDelete[0])
            removeMapPins(realmObjectToDelete[0])
            println("Deleting......")
            
            // delete object from realm
            self.realm.write {
                self.realm.delete(realmObjectToDelete)
            }
            self.tableViewOutlet.removeRowsAtIndexes(NSIndexSet(index:self.tableViewOutlet.selectedRow),
                withAnimation: NSTableViewAnimationOptions.SlideRight)
            
            self.hasTableLoaded = false
            self.tableViewOutlet.reloadData()
        }
    }
    
    
    func tableViewSelectionDidChange(NSNotification) {
        println("selection changed")
        //if self.isNoteOpen {
            self.defaults.setValue(SelectedRow()?.ssid, forKey: "CurrentSelectedNetwordSSID")
        //}
        if isNoteOpen {
            self.addNotePopover.performClose(self)
            self.isNoteOpen = false
        }
        if self.isMapOpen && SelectedRow() != nil {
            var wifiLocation = SelectedRow()!.location as String
            
            // need to add handling here if they don't have a valid location
            var LocationArray = split(wifiLocation) {$0 == ","}
            var LatFromArray = CLLocationDegrees(NSNumberFormatter().numberFromString(LocationArray[0])!.doubleValue)
            var LonFromArray = CLLocationDegrees(NSNumberFormatter().numberFromString(LocationArray[1])!.doubleValue)
            
            // center the map on the selected network
            centerMapOnLocation(CLLocation(latitude: LatFromArray, longitude: LonFromArray))
        }
    }
    
    
    func generateMapPins(rowdata: SSIDObj) {
        var LocationArray = split(rowdata.location) {$0 == ","}
        var LatFromArray = CLLocationDegrees(NSNumberFormatter().numberFromString(LocationArray[0])!.doubleValue)
        var LonFromArray = CLLocationDegrees(NSNumberFormatter().numberFromString(LocationArray[1])!.doubleValue)
        var coordToReturn: CLLocationCoordinate2D = CLLocationCoordinate2DMake(LatFromArray, LonFromArray)
        
        // place a pin there
        var objectAnnotation = MapDisplayData(ssid: rowdata.ssid, IP: rowdata.publicIP, coordinate: coordToReturn)
        objectAnnotation.color = MKPinAnnotationColor.Purple
        self.knownWifiLocations.append(objectAnnotation)
        self.mapview.addAnnotation(objectAnnotation)
    }
    
    
    func removeMapPins(rowdata: SSIDObj) {
        for annotation in self.knownWifiLocations {
            var title = String(stringInterpolationSegment: annotation.title) as String
            println(title)
            if title == String("Optional(SSID: " + rowdata.ssid + ")") {
                println("success")
                self.mapview.removeAnnotation(annotation)
            }
            else {
                println("NOT success")
            }
            
        }
    }
    
    override func viewDidDisappear() {
        self.isNoteOpen = false
        self.addNotePopover.performClose(self)
        
        // remove realm notification
        self.realm.removeNotification(self.notificationToken!)
        
        
        // handle closing the map if it's open
        if !self.mapview.hidden {
            self.appDelegate.expandWindow()
            self.mapview.hidden = true
            self.isMapOpen = false
        }
    }
    
    
    @IBAction func importRealm(sender: NSButton) {
        self.csv.CSVToRealm()
        // I need to wait to fire this, it's going before the data is loaded... mayble look at an event or notification
    }
    
    @IBAction func exportRealm(sender: NSButton) {
        self.csv.realmToCSV(realm)
        //self.refreshTableView()
    }
    
    
    @IBAction func viewMap(sender: NSButton) {
        println("open the map")
        self.appDelegate.expandWindow()
        self.viewOutlet.frame = (self.appDelegate.window.contentView as! NSView).bounds
        if self.mapview.hidden {
        self.mapview.hidden = false
        self.isMapOpen = true
        }
        else {
            self.mapview.hidden = true
            self.isMapOpen = false
        }
        
        // if a row is selected, otherwise we'll use the user's location
        if SelectedRow() != nil {
            // get wifi location from realm
            var wifiLocation = SelectedRow()!.location as String
            
            // need to add handling here if they don't have a valid location
            var LocationArray = split(wifiLocation) {$0 == ","}
            var LatFromArray = CLLocationDegrees(NSNumberFormatter().numberFromString(LocationArray[0])!.doubleValue)
            var LonFromArray = CLLocationDegrees(NSNumberFormatter().numberFromString(LocationArray[1])!.doubleValue)
            
            // center the map on the selected network
            centerMapOnLocation(CLLocation(latitude: LatFromArray, longitude: LonFromArray))
        }
        else {
            if mapview.userLocation.location != nil {
                centerMapOnLocation(mapview.userLocation.location)
            }
            else {
                let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
                centerMapOnLocation(initialLocation)
            }
        }
    }
    
    func refreshTableView() {
        self.tableViewOutlet.reloadData()
        if self.realm.objects(SSIDObj).count != 0 {
        tableView(self.tableViewOutlet, viewForTableColumn: NSTableColumn?(),row: self.realm.objects(SSIDObj).count)
            println("refresh")
        }
    }
    
    
    func setExportValue(sender:NSButton) {
        self.hasTableLoaded = false
        println(sender.tag)
        var editRow = SelectedRow(sender.tag)!
        var toSave = SSIDObj()
        toSave.ssid = editRow.ssid
        toSave.password = editRow.password
        toSave.security = editRow.security
        toSave.location = editRow.location
        toSave.bssid = editRow.bssid
        toSave.publicIP = editRow.publicIP
        toSave.Notes = editRow.Notes
        toSave.DateAdded = editRow.DateAdded
        toSave.DateExported = editRow.DateExported
        println(toSave.ssid + " is " + String(sender.state))
        if editRow.Export {
            toSave.Export = false
            println("settings to false")
        }
        else {
            toSave.Export = true
            println("settings to true")
        }
        self.realm.write {
            self.realm.add(toSave, update: true)
        }

    }
    
    
    
    // UI for second tab
    @IBAction func displayDetails(sender: NSButton) {
        var tempArray = self.appDelegate.findDetailed()
        for i in tempArray {
            self.detailsLabel.stringValue += (i + "\n")
        }
    }
    
    
    
    
    
    
    
    
    
    
    
}


    // MARK: - NSTableViewDataSource
    extension MasterViewController: NSTableViewDataSource {
        
        func numberOfRowsInTableView(aTableView: NSTableView) -> Int {
            var resultsAll = self.realm.objects(SSIDObj)
            return resultsAll.count
        }
        
        func tableView(tableView: NSTableView, viewForTableColumn tableColumn: NSTableColumn?, row: Int) -> NSView?
        {
            var cellView = tableView.makeViewWithIdentifier("SSID", owner: self) as! NSTableCellView
            var resultsAll = self.realm.objects(SSIDObj)
                if tableColumn?.identifier == "SSID" {
                    cellView.textField!.stringValue = resultsAll[row].ssid as String
                }
                else if tableColumn?.identifier == "Password" {
                    cellView.textField!.stringValue = resultsAll[row].password as String
                }
                else if tableColumn?.identifier == "BSSID" {
                    cellView.textField!.stringValue = resultsAll[row].bssid as String
                }
                else if tableColumn?.identifier == "Security" {
                    cellView.textField!.stringValue = resultsAll[row].security as String
                }
                else if tableColumn?.identifier == "Location" {
                    cellView.textField!.stringValue = resultsAll[row].location as String
                    generateMapPins(resultsAll[row])
                }
                else if tableColumn?.identifier == "IP" {
                    cellView.textField!.stringValue = resultsAll[row].publicIP as String
                }
                else if tableColumn?.identifier == "Export" {
                    if !self.hasTableLoaded {
                        var buttonCheck = NSButton(frame: cellView.bounds)
                        buttonCheck.setButtonType(NSButtonType.SwitchButton)
                        buttonCheck.tag = row
                        buttonCheck.action = "setExportValue:"
                        buttonCheck.target = self
                        
                        if resultsAll[row].Export {
                            buttonCheck.state = 1
                        }
                        else {
                            buttonCheck.state = 0
                        }
                        
                        buttonCheck.title = ""
                        cellView.textField!.stringValue = ""
                        if row < numberOfRowsInTableView(self.tableViewOutlet) {
                            cellView.addSubview(buttonCheck)
                        }
                    }
                }
                else {
                    cellView.textField!.stringValue = "1"
                }
            return cellView
        }
        
        func SelectedRow() -> SSIDObj? {
            let selectedRow = self.tableViewOutlet.selectedRow
            if selectedRow >= 0 && selectedRow < numberOfRowsInTableView(tableViewOutlet) {
                return self.realm.objects(SSIDObj)[selectedRow]
            }
            return nil
        }
        
        
        
        func SelectedRow(row: Int) -> SSIDObj? {
            let selectedRow = row
            if selectedRow >= 0 && selectedRow < numberOfRowsInTableView(tableViewOutlet) {
                return self.realm.objects(SSIDObj)[selectedRow]
            }
            return nil
        }
    }

    // MARK: - NSTableViewDelegate
    extension MasterViewController: NSTableViewDelegate {
    }
    
    
    

