//
//  CSVHandler.swift
//  Wifi Summoner
//
//  Created by Andrew Scott on 8/4/15.
//  Copyright (c) 2015 Drowned Coast. All rights reserved.
//

import Foundation
import RealmSwift
import Cocoa

class CSVHandler {
    
    let appDelegate = NSApplication.sharedApplication().delegate as! AppDelegate
    let realm = Realm() // Create realm pointing to default file
    let validHeaders: [String] = ["ssid", "password", "security", "lat", "lon", "bssid", "ip", "notes", "dateaddedUTC", "dateexportedUTC"]
    
    
    
    
    func realmToCSV(realm: Realm) {
        var totalRowsInRealm = realm.objects(SSIDObj).count
        var ssidObjs = realm.objects(SSIDObj)
        var csvtxt = "ssid,password,security,lat,lon,bssid,ip,notes,dateaddedUTC,dateexportedUTC\n"
        println("test ______++++++++++___________")
        let exportDate: String = NSDate().descriptionWithLocale(nil)!
        
        for i in 0...(totalRowsInRealm-1) {
            if ssidObjs[i].Export {
                var addDate: String = ssidObjs[i].DateAdded
            csvtxt += (ssidObjs[i].ssid + "," + ssidObjs[i].password + "," + ssidObjs[i].security + "," + ssidObjs[i].location + "," + ssidObjs[i].bssid + "," + ssidObjs[i].publicIP + "," + ssidObjs[i].Notes + ",")
            csvtxt += (addDate + "," + exportDate + "\n")
            }
        }
        var openPanel = NSSavePanel()
        var fileLocation =  NSFileManager.defaultManager().URLsForDirectory(.DesktopDirectory, inDomains: .UserDomainMask).first as! NSURL
        openPanel.nameFieldStringValue = "wifisummoner_dump.csv"

        openPanel.canCreateDirectories = true
        openPanel.beginWithCompletionHandler { (result) -> Void in
            if result == NSFileHandlingPanelOKButton {
                
                fileLocation = openPanel.directoryURL!
                
            }
            var fileDestinationUrl = fileLocation.URLByAppendingPathComponent(openPanel.nameFieldStringValue)
            csvtxt.writeToURL(fileDestinationUrl, atomically: true, encoding: NSUTF8StringEncoding, error: nil)
        }
    }
    
    
    
    
    
    
    func CSVToRealm() {
        var importArray: [[String]] = [[]]
        var noNil: Bool = true
        
        var error: NSErrorPointer = nil
        var openPanel = NSOpenPanel()
        var csvURL =  NSFileManager.defaultManager().URLsForDirectory(.DesktopDirectory, inDomains: .UserDomainMask).first as! NSURL
        openPanel.canChooseDirectories = false
        openPanel.canCreateDirectories = false
        openPanel.nameFieldLabel = "Select csv to Import"
        openPanel.canChooseFiles = true
        let extensions = "csv"
        let types = extensions.pathComponents
        openPanel.allowedFileTypes = types
        openPanel.beginWithCompletionHandler { (result) -> Void in
            if result == NSFileHandlingPanelOKButton {
                csvURL = openPanel.URL!
                //csvURL.  openPanel.nameFieldLabel
                println(csvURL)

            }
            let csv = CSV(contentsOfURL: csvURL, error: error)
            
            // Rows
            let rows = csv!.rows
            // total rows in csv
            var rowCount = (csv!.rows.count)
            // rows added
            var addCount = 0
            // rows updated
            var updateCount = 0
            if rowCount > 0 {rowCount -= 1}
            println(rowCount)
            let headers = csv!.headers  //=> header data, we'll check this before we add to the realm
            println(headers)
            if headers == self.validHeaders {
                
                for line in 0...rowCount {
                    
                    for lineItem in self.validHeaders {
                        if !self.testNotNil(rows[line][lineItem]! as String) {
                           noNil = false
                        }
                    }
                    
                    println(rows[line]["ssid"])
                    println(rows[line]["password"])
                    println(rows[line]["security"])
                    println(rows[line]["lat"])
                    println(rows[line]["lon"])
                    println(rows[line]["bssid"])
                    println(rows[line]["ip"])
                    println(rows[line]["notes"])
                    println(rows[line]["dateaddedUTC"])
                    println(rows[line]["dateexportedUTC"])
                    
                    println("tests-----------")
                    println("valid lat?")
                    println(self.testLatValidity(rows[line]["lat"]!))
                    println("valid lon?")
                    println(self.testLonValidity(rows[line]["lon"]!))
                    println("no nils?")
                    println(noNil)
                    
                    if self.testLatValidity(rows[line]["lat"]!) && self.testLonValidity(rows[line]["lon"]!) && noNil {
                        var toSave = SSIDObj()
                        toSave.ssid = rows[line]["ssid"]!
                        toSave.password = rows[line]["password"]!
                        toSave.security = rows[line]["security"]!
                        toSave.location = self.createLocation(rows[line]["lat"]!, lon: rows[line]["lon"]!)
                        toSave.bssid = rows[line]["bssid"]!
                        toSave.publicIP = rows[line]["ip"]!
                        toSave.Export = true
                        toSave.Notes = rows[line]["notes"]!
                        self.realm.write {
                            
                            if self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",rows[line]["ssid"]!)).count > 0 {
                                var modDate = NSDate(string: rows[line]["dateexportedUTC"]!)
                                var objToUpdate = self.realm.objects(SSIDObj).filter(NSPredicate(format: "ssid = %@",rows[line]["ssid"]!))
                                if (modDate?.earlierDate(NSDate(string:objToUpdate[0].DateAdded)!) == NSDate(string:objToUpdate[0].DateAdded)!) {
                                    updateCount += 1
                                }
                                else {
                                    println("one record is too old")
                                }
                            }
                            else {
                                addCount += 1
                            }
                            self.realm.add(toSave, update: true)
                        }
                    }
                    else {
                        // handle popup error
                        let alert = NSAlert()
                        alert.messageText = "Warning"
                        alert.addButtonWithTitle("OK")
                        alert.informativeText = "File contains missing data or is corrupt: file format error."
                        
                        alert.beginSheetModalForWindow(self.appDelegate.window, completionHandler: nil)
                    }
                }
                let successAlert = NSAlert()
                successAlert.messageText = "Import Successful"
                successAlert.addButtonWithTitle("OK")
                if (addCount + updateCount) > 1 {
                    successAlert.informativeText = ("Successfully added: " + String(addCount + updateCount) + " records")
                }
                else if (addCount + updateCount) == 1 {
                    successAlert.informativeText = ("Successfully added: " + String(addCount + updateCount) + " record")
                }
                
                successAlert.beginSheetModalForWindow(self.appDelegate.window, completionHandler: nil)
            }
            else {
                // handle popup error
                let alert = NSAlert()
                alert.messageText = "Warning"
                alert.addButtonWithTitle("OK")
                alert.informativeText = "File headers don't match: file format error."
                
                alert.beginSheetModalForWindow(self.appDelegate.window, completionHandler: nil)
                
            }

        }

    }
    
    
    func createLocation(lat: String, lon: String) -> String {
        var locationString = String(lat) + "," + String(lon)
        return locationString
    }
    
    
    
    
    
    // test file contents for validity
    func testLatValidity(latToTest: String) -> Bool {
        var latAsDouble = (latToTest as NSString).doubleValue
        if latAsDouble >= -90.0 && latAsDouble <= 90.0 {
            return true
        }
        return false
    }
    
    func testLonValidity(lonToTest: String) -> Bool {
        var lonAsDouble = (lonToTest as NSString).doubleValue
        if lonAsDouble >= -180.0 && lonAsDouble <= 180.0 {
            return true
        }
        return false
    }
    
    func testNotNil(rowItem: AnyObject?) -> Bool {
        if  rowItem != nil {
            return true
        }
        return false
    }
    
}