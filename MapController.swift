//
//  MapController.swift
//  Wifi Summoner
//
//  Created by Andrew Scott on 7/26/15.
//  Copyright (c) 2015 Drowned Coast. All rights reserved.
//

import Cocoa

class MapController: NSWindowController {

    @IBOutlet var mapWindow: NSWindow!
    
    override func windowDidLoad() {
        super.windowDidLoad()
        mapWindow.makeKeyAndOrderFront(self)
        println("I should open")
        
    }
    
}
