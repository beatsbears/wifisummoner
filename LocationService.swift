//
//  LocationService.swift
//  Wifi Summoner
//
//  Created by Andrew Scott on 7/24/15.
//  Copyright (c) 2015 Drowned Coast. All rights reserved.
//

import Foundation
import CoreLocation

class LocationService: CLLocationManager, CLLocationManagerDelegate {
    
    var locationCurrent = CLLocationCoordinate2DMake(0, 0)
    var authorizedToUse: Bool = false
    var errorOccurred = false
    let MaxLocationFixStaleness = 10.0
    var locationString = ""
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
        // the last one will be the most recent
        let updatedLocation = locations.last as? CLLocation;
        
        // Check is not older than 10 seconds, otherwise discard it
        if (updatedLocation!.timestamp.timeIntervalSinceNow < MaxLocationFixStaleness) {
            locationCurrent = updatedLocation!.coordinate;
            CFRunLoopStop(CFRunLoopGetCurrent());
        }
        
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        errorOccurred = true;
        CFRunLoopStop(CFRunLoopGetCurrent());
    }
    
    func execute() {
        switch CLLocationManager.authorizationStatus() {
        case .Restricted:
            errorOccurred = true;
            println("restrict")
        case .Denied:
            errorOccurred = true;
            println("denied")
        default:
            if (!CLLocationManager.locationServicesEnabled()) {
                errorOccurred = true;
            }
        }
        var status: Int32 = 0
        let manager = CLLocationManager()
        manager.delegate = self;
        manager.startUpdatingLocation()
        status = CFRunLoopRunInMode(kCFRunLoopDefaultMode, 15, 0)
        if (!errorOccurred) {
            println(status)
            displayLatLon(locationCurrent)
            self.locationString = displayLatLon(locationCurrent)
        }
    }
    
    func displayLatLon(location: CLLocationCoordinate2D) -> String {
        var lat = String(stringInterpolationSegment: location.latitude)
        if lat != "0.0" {
            var reduceAccuracyLat = advance(lat.endIndex, -6)
            lat = lat.substringToIndex(reduceAccuracyLat)
        }
        var lon = String(stringInterpolationSegment: location.longitude)
        if lon != "0.0" {
            var reduceAccuracyLon = advance(lon.endIndex, -6)
            lon = lon.substringToIndex(reduceAccuracyLon)
        }
        println(lat + "," + lon)
        return lat + "," + lon
    }
}


